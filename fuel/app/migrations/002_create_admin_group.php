<?php

namespace Fuel\Migrations;

/**
 * Create admin groups
 *
 * @since 1.0.0
 * @author Pham Quynh Anh
 */
class Create_admin_group
{

	/**
	 * Create admin group
	 *  group: admin
	 *  role: admin
	 *	filter A
	 *
	 * @param void
	 * @return void
	 *
	 * @since 1.0.0
	 * @version 1.0.0
	 *
	 * @access public
	 * @author Pham Quynh Anh
	 */
	public function up()
	{
		\DB::start_transaction();

		try
		{
			// admin role
			$role = \Model\Auth_Role::forge(['name' => 'admin', 'filter' => 'A']);
			$role->save();

			// admin group
			$group = \Model\Auth_Group::forge(['name' => 'admin']);
			$group->roles[] = $role;
			$group->save();
		}
		catch (Exception $e)
		{
			\DB::rollback_transaction();
			throw $e;
		}

		\DB::commit_transaction();
	}

	/**
	 * Remove admin group
	 *
	 * @param void
	 * @return void
	 *
	 * @since 1.0.0
	 * @version 1.0.0
	 *
	 * @access public
	 * @author Pham Quynh Anh
	 */
	public function down()
	{
		$group = \Model\Auth_Group::find_by_name('admin');
		$group->delete();
		$role = \Model\Auth_Role::find_by_name('admin');
		$role->delete();
	}

}
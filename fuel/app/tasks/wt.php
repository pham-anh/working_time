<?php

namespace Fuel\Tasks;

/**
 * Set up the project
 *
 * @since 1.0.0
 * @version 1.0.0
 * @author Pham Quynh Anh <pqa.dev@gmail.com>
 */
class Wt
{

	/**
	 * show help
	 *
	 * @since 1.0.0
	 * @version 1.0.0
	 * @author Pham Quynh Anh <pqa.dev@gmail.com>
	 */
	public function run()
	{
		$this->help();
	}

	/**
	 * show help
	 *
	 * @since 1.0.0
	 * @version 1.0.0
	 * @author Pham Quynh Anh <pqa.dev@gmail.com>
	 */
	public function help()
	{
		print <<<EOT
  php oil wt[:setup] [:teardown] [:help]

Usage
  php oil wt:setup      Setup the app
  php oil wt:teardown   Teardown the app
  php oil wt:help       Show help


EOT;
	}

	/**
	 * setup
	 *
	 * @since 1.0.0
	 * @version 1.0.0
	 * @author Pham Quynh Anh <pqa.dev@gmail.com>
	 */
	public function setup()
	{
		// run packages and app migrations
		print "- Run migrations.\r\n";
		print(shell_exec("php oil r migrate --packages=auth"));
		print(shell_exec("php oil r migrate"));

		print "Completed!\r\n";
	}

	/**
	 * teardown
	 *
	 * @since 1.0.0
	 * @version 1.0.0
	 * @author Pham Quynh Anh <pqa.dev@gmail.com>
	 */
	public function teardown()
	{
		// down packages and app migrations (need to down to have config/Fuel::ENV/migration.php to be updated by FuelPHP
		print "- Down migrations.\r\n";
		print(shell_exec("php oil r migrate:down --version=0"));
		print(shell_exec("php oil r migrate:down --packages=auth --version=0"));

		print "Completed!\r\n";
		
	}
}

<?php

namespace Fuel\Tasks;

/**
 * Make symbolic link from assets in fuel/vendor/ to html/assets/vendor
 *
 * @since 1.0.0
 * @version 1.0.0
 * @author Pham Quynh Anh <pqa.dev@gmail.com>
 */
class Vendor_Assets
{

	protected $vendor_assets = array(
		'twbs/bootstrap/dist/css',
		'twbs/bootstrap/dist/js');
	/**
	 * create symlink from VENDORPATH to DOCROOT.'assets/vendor/'
	 *
	 * @since 1.0.0
	 * @version 1.0.0
	 * @author Pham Quynh Anh <pqa.dev@gmail.com>
	 */
	public function link()
	{
		foreach ($this->vendor_assets as $asset)
		{
			if (symlink(VENDORPATH.$asset, DOCROOT.'html/assets/vendor/'.$asset))
			{
				\Cli::write("\t".'Made symlink from: '.VENDORPATH.$asset.' to '.DOCROOT.'html/assets/vendor/'.$asset, 'green');
			}
			else
			{
				\Cli::write("\t".'Failed to make symlink from: '.VENDORPATH.$asset.' to '.DOCROOT.'html/assets/vendor/'.$asset, 'red');
			}
		}
	}

	/**
	 * delete symbolic link in html/assets/vendor/
	 *
	 * @since 1.0.0
	 * @version 1.0.0
	 * @author Pham Quynh Anh <pqa.dev@gmail.com>
	 */
	public function unlink()
	{
		foreach ($this->vendor_assets  as $asset)
		{
			if ( ! is_link(DOCROOT.'html/assets/vendor/'.$asset)) {
				\Cli::write("\t".DOCROOT.'html/assets/vendor/'.$asset.' is not a symlink', 'red');
				return;
			}

			$result = unlink(DOCROOT.'html/assets/vendor/'.$asset);
			if ($result == true) {
				\Cli::write("\t".'Deleted symlink '.DOCROOT.'html/assets/vendor/'.$asset, 'green');
			}
			else
			{
				\Cli::write("\t".'Failed to delete symlink '.DOCROOT.'html/assets/vendor/'.$asset, 'red');
			}
		}
	}

}
